# Nutsinee Kijbunchoo Sep 28, 2018
#
# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_FREQ.py $
import sys
import time
from guardian import GuardState, GuardStateDecorator, NodeManager

#############################################
#nodes
#nodes = NodeManager('SQZ_PLL')
nominal = 'LOCKED'

#############################################
#Function

def FREQ_locked():
    return ezca['SQZ-OPO_SERVO_FASTMON'] < 5

def in_fault():
    if not FREQ_locked():
        return True

class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_fault():
            return 'DOWN'

#############################################
#States

class INIT(GuardState):
    index = 0

    def main(self):
        if FREQ_locked():
            return 'LOCKED'
        else:
            return 'IDLE'
        

class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SQZ-VCO_CONTROLS_ENABLE'] = 0
        ezca.switch('SQZ-OPO_FREQ', 'OUTPUT', 'OFF')
        ezca['SQZ-OPO_FREQ_RSET'] = 2 #clear history
        ezca['SQZ-OPO_FREQ_GAIN'] = 0
        ezca.switch('SQZ-OPO_FREQ', 'INPUT', 'OFF')
        ezca.switch('SQZ-OPO_FREQ', 'OUTPUT', 'ON')
        #ezca['SQZ-OPO_FREQ_TRAMP']=60

    def run(self):
        return True


class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        notify('Cannot lock OPO Freq. Check PLL lock.')
        log('Cannot lock OPO Freq. Check PLL lock.')
        if ezca['GRD-SQZ_PLL_STATE_N'] == 10:
            return 'LOCKED'

    
class LOCKING(GuardState):
    index = 5
    request = False

    def main(self):
        ezca['SQZ-OPO_FREQ_RSET'] = 2 #clear history


    def run(self):
        if (ezca['GRD-SQZ_PLL_STATE_N'] == 10) and (ezca['GRD-SQZ_OPO_STATE_N'] == 12):
            if abs(ezca['SQZ-OPO_SERVO_FASTMON']) > 0.5:
                notify('minimizing SERVO_FAST_MON')
                #fastmon = abs(ezca['SQZ-OPO_SERVO_FASTMON'])
                if ezca['SQZ-OPO_SERVO_FASTMON'] < 0:
                    notify('OPO PZT +')
                    ezca['SQZ-OPO_PZT_1_OFFSET'] = ezca['SQZ-OPO_PZT_1_OFFSET']+0.01 
                if ezca['SQZ-OPO_SERVO_FASTMON'] > 0:
                    notify('OPO PZT -')
                    ezca['SQZ-OPO_PZT_1_OFFSET'] = ezca['SQZ-OPO_PZT_1_OFFSET']-0.01 

            else:
                notify('OPO_SERVO_FASTMON OK. Moving on') 
                ezca['SQZ-OPO_FREQ_GAIN'] = 25 #gotta start small or PLL will rail every time
                ezca['SQZ-OPO_FREQ_RSET'] = 2
                ezca.switch('SQZ-OPO_FREQ', 'INPUT', 'ON') #turn on filters
                ezca.switch('SQZ-OPO_FREQ', 'OUTPUT', 'ON')
                time.sleep(3)

                return True

        else:
            notify('PLL or OPO not nominal')
            log('PLL or OPO not nominal')
            return 'IDLE'

class LOCKED(GuardState):
    index = 10


    #add some checker
    @fault_checker
    def run(self):
        return True

#############################################

edges = [
    ('INIT', 'IDLE'),
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'LOCKING'),
    ('LOCKING', 'LOCKED'),
    ('LOCKED','IDLE')

]


